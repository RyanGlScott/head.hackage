# To update nixpkgs bump rev and baseNixpkgs's sha256.
let
  rev = "c2fd152c98dc618c9c06b1551faee17e79a03b7f";
in
fetchTarball {
  url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  sha256 = "07cafslsgdymzcfcpgzw8fqv07r4wslsz7xijni1l1n2agcj2436";
}

